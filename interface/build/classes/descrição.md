O jogo terá a tela principal com os botões de jogar e ajuda.
No botão de jogar terá as opções de jogo.
No botão de ajuda terão as instruções de jogo.

Instruções para jogar:

A cobra não pode atravessar as barreira da tela ou tocar em sua calda se ela fizer isso morrerá e o jogo acaba.
Assim que o usuario clicar em jogar parecerá três opções ,elas são os tipos de cobras que o usuario poderá jogar.
A opções são:
1.Comum: A Snake classica, sem habilidades especiais.
2.Kitty : Essa Snake tem as habilidades de atravessar as barreiras do jogo, mas não pode atravessar as bordas nem a si mesma.
3.Star : Recebe o dobro de pontos ao comer as frutas.
Para fazer a cobra crescer basta fazer  ela comer as frutas forneciadas.
Assim que o jogador escolher a opção de snake uma tela do jogo com a snake escolhida será aberta para começar o jogo.
No jogo haverá 4 tipos de frutas e o jogador precisa ter cuidado ao fazer sua cobra comer elas.
As 4 tipos são :
1. Simple Fruit:(Apple) Fruta comum, dá um ponto e aumenta o tamanho da cobra.
2. Bomb Fruit: (Cashew)Essa fruta deve levar a morte da Snake.
3. Big Fruit: (wait) Dá o dobro de pontos da Simple Fruit e aumenta o tamanho da cobra da mesma forma que a Simple Fruit.
4.Decrease Fruit:(Orange) Diminui o tamanho da cobra para o tamanho inicial, sem fornecer nem retirar pontos.

