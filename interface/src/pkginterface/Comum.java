
package pkginterface;
import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 *
 * @author dani
 */
public class Comum extends JFrame {

    public Comum() {
        
        initUI();}

    private void initUI() {
         add(new Main());
        
        setResizable(false);
        pack();
        
        setTitle("Comum");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }


    /**
     * @param args the command line arguments
     */
   public static void main(String[] args) {
        
         EventQueue.invokeLater(() -> {
            JFrame ex = new Comum();
            ex.setVisible(true);
        });
    
   }
}