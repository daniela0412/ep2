package pkginterface;
import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 *
 * @author dani
 */
public class Star extends JFrame{
    
    public Star() {
        
        initUI();
    }

    private void initUI() {
         add(new Main3());
        
        setResizable(false);
        pack();
        
        setTitle("Star");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        EventQueue.invokeLater(() -> {
            JFrame ex = new Comum();
            ex.setVisible(true);
        });
        
    }
}

   
