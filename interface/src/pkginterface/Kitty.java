package pkginterface;
import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 *
 * @author dani
 */
public class Kitty extends JFrame{
    
    
    public Kitty() {
        
        initUI();
    }

    private void initUI() {
         add(new Main2());
        
        setResizable(false);
        pack();
        
        setTitle("Kitty");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
         EventQueue.invokeLater(() -> {
            JFrame ex = new Comum();
            ex.setVisible(true);
        });
    
    }
    
}
