package pkginterface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
/**
 *
 * @author dani
 */
public class Main3 extends JPanel implements ActionListener{
    
     private final int B_WIDTH = 400;
    private final int B_HEIGHT = 300;
    private final int DOT_SIZE = 10;
    private final int ALL_DOTS = 900;
    private final int RAND_POS = 29;
    private final int DELAY = 140;

    private final int x[] = new int[ALL_DOTS];
    private final int y[] = new int[ALL_DOTS];

    private int dots;
    private int apple_x;
    private int apple_y;
    private int wait_x;
    private int wait_y;
    private int orange_x;
    private int orange_y;
    private int cashew_x;
    private int cashew_y;

    private boolean leftDirection = false;
    private boolean rightDirection = true;
    private boolean upDirection = false;
    private boolean downDirection = false;
    private boolean inGame = true;

    private Timer timer;
    private Image ball;
    private Image apple;
    private Image wait;
    private Image orange;
    private Image cashew;
    private Image head;

    public Main3() {
        
        initMain3();
    }
    
    private void initMain3() {

        addKeyListener(new TAdapter());
        setBackground(Color.black);
        setFocusable(true);

        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));
        loadImages();
        initGame();
    }

    private void loadImages() {

        ImageIcon iid = new ImageIcon("src/resources/dot.png");
        ball = iid.getImage();

        ImageIcon iia = new ImageIcon("src/resources/apple.jpg");
        apple = iia.getImage();
        ImageIcon iib = new ImageIcon("src/resources/wait.jpg");
        wait= iib.getImage();
        ImageIcon iic = new ImageIcon("src/resources/orange.jpg");
        orange = iic.getImage();
        ImageIcon iie = new ImageIcon("src/resources/cashew.jpg");
        cashew = iie.getImage();

        ImageIcon iih = new ImageIcon("src/resources/head.png");
        head = iih.getImage();
    }

    private void initGame() {

        dots = 2;

        for (int z = 0; z < dots; z++) {
            x[z] = 50 - z * 10;
            y[z] = 50;
        }
        
        locateApple();
        locateWait();
        locateOrange();
        locateCashew();

        timer = new Timer(DELAY, this);
        timer.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        doDrawing(g);
    }
    
    private void doDrawing(Graphics g) {
        
        if (inGame) {

            g.drawImage(apple, apple_x, apple_y, this);
             g.drawImage(wait, wait_x, wait_y, this);
             g.drawImage(orange, orange_x, orange_y, this);
             g.drawImage(cashew, cashew_x, cashew_y, this);

            for (int z = 0; z < dots; z++) {
                if (z == 0) {
                    g.drawImage(head, x[z], y[z], this);
                } else {
                    g.drawImage(ball, x[z], y[z], this);
                }
            }

            Toolkit.getDefaultToolkit().sync();

        } 
          
        
        
        else {

            gameOver(g);
        }        
    }
       
            
    private void gameOver(Graphics g) {
        
        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metr = getFontMetrics(small);

        g.setColor(Color.white);
        g.setFont(small);
        g.drawString(msg, (B_WIDTH - metr.stringWidth(msg)) / 2, B_HEIGHT / 2);
    }

    private void checkApple() {

        if ((x[0] == apple_x) && (y[0] == apple_y)) {

            dots+=2;
            locateApple();
        }
    }
     private void checkWait() {
          if ((x[0] == wait_x) && (y[0] == wait_y)) {

            dots+=4;
            locateWait();
        
        }
        
    }

    private void checkOrange() {
         if ((x[0] == orange_x) && (y[0] == orange_y)) {

            dots=2;
            locateOrange();
        }
    }
    
    private void checkCashew(){
        if((x[0]== cashew_x) && (y[0]== cashew_y)){
           
            
            dots = - 4;
        
            locateCashew();
        }
    
    }

    private void move() {

        for (int z = dots; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }

        if (leftDirection) {
            x[0] -= DOT_SIZE;
        }

        if (rightDirection) {
            x[0] += DOT_SIZE;
        }

        if (upDirection) {
            y[0] -= DOT_SIZE;
        }

        if (downDirection) {
            y[0] += DOT_SIZE;
        }
        
    }

    private void checkCollision() {

        for (int z = dots; z > 0; z--) {

            if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                inGame = false;
            }
        }

        if (y[0] >= B_HEIGHT) {
            inGame = false;
        }

        if (y[0] < 0) {
            inGame = false;
        }

        if (x[0] >= B_WIDTH) {
            inGame = false;
        }

        if (x[0] < 0) {
            inGame = false;
        }
        
        if (!inGame) {
            timer.stop();
        }
    }

    private void locateApple() {

        int r = (int) (Math.random() * RAND_POS);
        apple_x = ((r * DOT_SIZE));

        r = (int) (Math.random() * RAND_POS);
        apple_y = ((r * DOT_SIZE));
    }
     private void locateWait() {

        int r = (int) (Math.random() * RAND_POS);
        wait_x = ((r * DOT_SIZE));

        r = (int) (Math.random() * RAND_POS);
        wait_y = ((r * DOT_SIZE));
    }
      private void locateOrange() {

        int r = (int) (Math.random() * RAND_POS);
        orange_x = ((r * DOT_SIZE));

        r = (int) (Math.random() * RAND_POS);
        orange_y = ((r * DOT_SIZE));
    }
      private void locateCashew(){
         
        int r = (int) (Math.random()*RAND_POS);
        cashew_x = ((r * DOT_SIZE));
        
        r = (int) (Math.random() * RAND_POS);
        cashew_y = ((r * DOT_SIZE));
      }

    /**
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (inGame) {

            checkApple();
            checkWait();
            checkOrange();
            checkCashew();
            checkCollision();
            move();
        }

        repaint();
    }

    
    private class TAdapter extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {

            int key = e.getKeyCode();

            if ((key == KeyEvent.VK_LEFT) && (!rightDirection)) {
                leftDirection = true;
                upDirection = false;
                downDirection = false;
            }

            if ((key == KeyEvent.VK_RIGHT) && (!leftDirection)) {
                rightDirection = true;
                upDirection = false;
                downDirection = false;
            }

            if ((key == KeyEvent.VK_UP) && (!downDirection)) {
                upDirection = true;
                rightDirection = false;
                leftDirection = false;
            }

            if ((key == KeyEvent.VK_DOWN) && (!upDirection)) {
                downDirection = true;
                rightDirection = false;
                leftDirection = false;
            }
        }
    }
}
    
